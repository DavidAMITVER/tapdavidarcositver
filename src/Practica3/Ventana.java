package Practica4;

//Importaciones
import java.awt.GridLayout;
import javax.swing.JPanel;
import javax.swing.JFrame;
import java.awt.Dimension;
import javax.swing.JButton;

public class Ventana extends JFrame{
	//Atributos
	private JPanel panel;		
	//Metodos
	public Ventana(){
		this.setSize(300,500);
		this.setTitle("Calculadora");
		this.setLocationRelativeTo(null);
		//setLayout(new GridLayout(3,3,0,0));
		this.setMinimumSize(new Dimension(300,500));
		this.iniciarComponentes();
		this.setDefaultCloseOperation(EXIT_ON_CLOSE);
	}

	private void iniciarComponentes(){
		iniciarPanel();
		iniciarBotones();
	}
	private void iniciarPanel(){
		panel = new JPanel();
		panel.setLayout(new GridLayout(6,4,0,0));
		this.getContentPane().add(panel);//Agregamos el panel a la ventana...
	}
private void iniciarBotones(){
	panel.add(new JButton("%"));
	panel.add(new JButton("CE"));
	panel.add(new JButton("C"));
	panel.add(new JButton("<-x"));
	panel.add(new JButton("1/x"));
	panel.add(new JButton("X^2"));
	panel.add(new JButton("Raiz"));
	panel.add(new JButton("/"));
	panel.add(new JButton("7"));
	panel.add(new JButton("8"));
	panel.add(new JButton("9"));
	panel.add(new JButton("X"));
	panel.add(new JButton("4"));
	panel.add(new JButton("5"));
	panel.add(new JButton("6"));
	panel.add(new JButton("-"));
	panel.add(new JButton("1"));
	panel.add(new JButton("2"));
	panel.add(new JButton("3"));
	panel.add(new JButton("+"));
	panel.add(new JButton("+/-"));
	panel.add(new JButton("0"));
	panel.add(new JButton("."));
	panel.add(new JButton("="));
	}

}
