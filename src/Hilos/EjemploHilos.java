package Hilos;

import java.util.logging.Level;
import java.util.logging.Logger;


public class EjemploHilos {
    public static void main(String args[]){
        Thread thr = new Thread(new HiloRunnable());
        Thread thr1=new HiloThread();
        thr.start(); thr1.start();
        try {
            thr.join();
            thr1.join();
        } catch (InterruptedException ex) {
            Logger.getLogger(EjemploHilos.class.getName()).log(Level.SEVERE, null, ex);
        }
        System.out.println("La ejecución del main finalizo");
    }
}
class HiloRunnable implements Runnable {

    @Override
    public void run() {
        for(int i=0;i<20;i++){
            System.out.println("Buenos dias: "+i);
            try {
                Thread.sleep(1000);
            } catch (InterruptedException ex) {
                Logger.getLogger(HiloRunnable.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
       System.out.println("Termino el hilo Runnable");
    }
    
}
class HiloThread extends Thread {
     @Override
    public void run() {
        for(int i=100;i<120;i++){
            System.out.println("Buenos noches: "+i);
            try {
                Thread.sleep(2000);
            } catch (InterruptedException ex) {
                Logger.getLogger(HiloThread.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
        System.out.println("Termino el hilo Thread");
    }
}