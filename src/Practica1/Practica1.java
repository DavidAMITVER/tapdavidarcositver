import javax.swing.JFrame;
import javax.swing.JButton;
import java.awt.event.*;

public class Practica1 extends JFrame {
    
    JButton btnCerrar;
    
    /**
     Metodo constructor se puede poner sin especificar el modifcador de acceso
     * Como 
     * Practica1(){
     }
     * public Practica1(){
     * 
     * }
     */
    public Practica1(){
        this.setSize(300, 200);
        this.setTitle("Practica 1");
        
        btnCerrar = new JButton("Cerrar");
        MiActionListener ml = new MiActionListener();
        
        btnCerrar.addActionListener(ml);
        
        this.add(btnCerrar);
    }    
    
    public static void main(String args[]){
        Practica1 p = new Practica1();        
        p.setVisible(true);
        System.out.println("Programa corriendo");
    }    
}

class MiActionListener implements ActionListener{
    @Override
    public void actionPerformed(ActionEvent e) {
            System.exit(0);
    }    
}
