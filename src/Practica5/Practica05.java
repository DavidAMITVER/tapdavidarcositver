import javax.swing.JOptionPane;
import java.util.Random;
import javax.swing.JSpinner;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.SwingConstants;
import javax.swing.JFrame;
import javax.swing.JTextField;
import javax.swing.JButton;
import javax.swing.JPanel;
import javax.swing.JLabel;

public class Practica05 extends JFrame{
	//Atributos
	private JPanel panel;
	private JLabel label1;
	private JLabel label2;
	private JLabel label3;
	private JButton boton;
	private JSpinner Spinner1;
	private JSpinner Spinner2;
	private JTextField cajaTexto;
	//private JTextField cajaTexto;
	
	//Metodos
	public Practica05(){
		setSize(400,500);
		setTitle("Generador de numeros...");
		iniciarComponentes();
		setDefaultCloseOperation(EXIT_ON_CLOSE);
	}
	private void iniciarComponentes(){
		colocarPanel();
		colocarEtiquetas();
		colocarSpinner();
		colocarCajaTexto();
		colocarBoton();
	}
	private void colocarCajaTexto(){
		cajaTexto = new JTextField();
		cajaTexto.setText("0");
		cajaTexto.setBounds(200,250,150,40);
		panel.add(cajaTexto);
		
	}
	private void colocarSpinner(){
		Spinner1= new JSpinner();
		Spinner1.setBounds(250,50,100,40);
		panel.add(Spinner1);
		Spinner2=new JSpinner();
		Spinner2.setBounds(250,150,100,40);
		panel.add(Spinner2);
		eventoOyenteSpinner();
	}
	private void eventoOyenteSpinner(){
		ActionListener oyenteAccion = new ActionListener(){
		@Override
		public void actionPerformed(ActionEvent ae){
				if((int)Spinner1.getValue()>(int)Spinner2.getValue()){
					JOptionPane.showMessageDialog(rootPane,"el valor inferior debe ser menor al superior...","Error",JOptionPane.ERROR_MESSAGE);
				}
			}

		};
		//Spinner2.addActionListener(oyenteAccion);
	}

	private void colocarPanel(){
		panel = new JPanel();
		panel.setLayout(null);
		this.add(panel);
	}
	private void colocarBoton(){
		boton = new JButton("Generar!");   
		boton.setBounds(100,350,200,50);
		panel.add(boton);
		//Agregando el evento
		eventoAccion();
	}
	private void eventoAccion(){
	ActionListener oyenteAccion = new ActionListener(){
		@Override
		public void actionPerformed(ActionEvent ae){
				int min=0;
				int max=0;
				int numeroGenerado=0;
				min=(int)Spinner1.getValue();
				max=(int)Spinner2.getValue();
				Random random = new Random();
				try{
					numeroGenerado=random.nextInt(max-min)+min;
				}catch(Exception e){
					JOptionPane.showMessageDialog(rootPane,"Tiene que establecer un rango mayor a 0","Error",JOptionPane.ERROR_MESSAGE);
				}
					cajaTexto.setText(Integer.toString(numeroGenerado));
			}

		};
		boton.addActionListener(oyenteAccion);
	}
	private void colocarEtiquetas(){
		label1=new JLabel("Limite inferior: ");
		label1.setBounds(50,50,100,40);
		panel.add(label1);
		label2=new JLabel("Limite superior: ");
		label2.setBounds(50,150,100,40);
		panel.add(label2);
		label3=new JLabel("Numero generado : ");
		label3.setBounds(50,250,150,40);
		panel.add(label3);
	}

}
